<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $fillable    = ['name', 'gender', 'address', 'phone_number', 'id_card', 'email', 'isMember'];
    public    $timestamps  = true;
}
