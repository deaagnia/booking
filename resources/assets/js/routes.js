import index from './components/index.vue';
import login from './components/auth/login.vue';
import checkin from './components/contents/checkin.vue';
import rooms from './components/contents/rooms/rooms.vue';
import formRooms from './components/contents/rooms/form.vue';
import members from './components/contents/members/members.vue';
import detaulRoom from './components/contents/rooms/detail.vue';
import serviceroom from './components/contents/srooms/srooms.vue';
import detailCus from './components/contents/customers/detail.vue';
import formRoomService from './components/contents/srooms/form.vue';
import bookingRoom from './components/contents/booking/booking.vue';
import formCustomers from './components/contents/customers/form.vue';
import customers from './components/contents/customers/customers.vue';
import detailServiceRoom from './components/contents/srooms/detail.vue';
import formServiceMember from './components/contents/smembers/form.vue';
import detailServiceMember from './components/contents/smembers/detail.vue';
import serviceMember from './components/contents/smembers/service_members.vue';



const routes = [
    {
        path : '/',
        name : 'index',
        component : index,
    },
    {
        path : '/login',
        name : 'login',
        component : login,
    },
    {
        path : '/checkin',
        name : 'checkin',
        component : checkin,
    },
    {
        path : '/customers',
        name : 'customers',
        component : customers,
    },
    {
        path : '/form-customers',
        name : 'formCustomer',
        component : formCustomers,
    },
    {
        path : '/form-customers-edit/:id',
        name : 'formCustomerEdit',
        component : formCustomers,
    },
    {
        path : '/detail-customers/:id',
        name : 'detail-cus',
        component : detailCus,
    },
    {
        path : '/members',
        name : 'members',
        component : members,
    },
    {
        path : '/rooms',
        name : 'rooms',
        component : rooms,
    },
    {
        path : '/form-rooms',
        name : 'formRooms',
        component : formRooms,
    },
    {
        path : '/form-rooms/:id',
        name : 'formEditRooms',
        component : formRooms,
    },
    {
        path : '/detail-room/:id',
        name : 'detailRoom',
        component : detaulRoom,
    },
    {
        path : '/serviceMembers',
        name : 'serviceMember',
        component : serviceMember,
    },
    {
        path : '/form-service-members',
        name : 'formServiceMember',
        component : formServiceMember,
    },
    {
        path : '/form-edit-service-members/:id',
        name : 'formEditServiceMember',
        component : formServiceMember,
    },
    {
        path : '/detail-service-members/:id',
        name : 'detailServiceMember',
        component : detailServiceMember,
    },
    {
        path : '/servie-rooms',
        name : 'serviceRooms',
        component : serviceroom,
    },
    {
        path : '/form-service-room',
        name : 'formServiceRoom',
        component : formRoomService,
    },
    {
        path : '/form-edit-service-room/:id',
        name : 'formEditServiceRoom',
        component : formRoomService,
    },
    {
        path : '/detail-service-room/:id',
        name : 'detailServiceRoom',
        component : detailServiceRoom,
    },
    {
        path : '/booking/room',
        name : 'bookingRoom',
        component : bookingRoom,
    },
];

export default routes;