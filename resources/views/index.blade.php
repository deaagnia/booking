<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!--    Token    -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!--    Vue     -->
<!--    <link rel="stylesheet" href="{{ mix('css/app.css') }}">-->

    <!--    Template    -->
    <link rel="icon" href="/assets/images/logo.png" type="image/png" >
    <link rel="stylesheet" href="/assets/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/assets/css/style.css">
    <link rel="stylesheet" href="/assets/css/colors/blue.css" id="theme">
    <link rel="stylesheet" href="/assets/plugins/icheck/skins/all.css">



    <title>Boo.King!</title>
</head>
<body>

<div class="login-register" style="background-image:url(/assets/images/background/login-register.jpg)">
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"></circle>
        </svg>
    </div>

    <section id="wrapper">
        <div>
            <div class="login-box card">
                <div class="card-body">
                    <form method="post" class="form-horizontal form-material" id="loginform" action="{{ route('login') }}">
                        <div class="form-group">
                            <div class="col-xs-12">
                                <input type="email" name="email" placeholder="your@mail.com" class="form-control" v-model="auth.email">
                            </div>
                        </div>

                        {{ csrf_field() }}
                        <div class="form-group">
                            <div class="col-xs-12">
                                <input type="password" name="password" placeholder="yourpassword" class="form-control" v-model="auth.password">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-12">
                                <a href="" class="text-dark pull-right"><i class="fa fa-lock m-r-5"></i> Forgot Password?</a>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-xs-12">
                                <button type="submit" name="button" class="btn btn-info btn-block text-uppercase waves-effect waves-light">Login</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
</div>

    <!--    Vue JS  -->
<!--    <script src="{{ mix('js/app.js') }}"></script>-->

    <!--    Tempalte JS-->
    <script src="/assets/plugins/jquery/jquery.min.js"></script>
    <script src="/assets/plugins/bootstrap/js/popper.min.js"></script>
    <script src="/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="/assets/js/jquery.slimscroll.js"></script>
    <script src="/assets/js/waves.js"></script>
    <script src="/assets/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="/assets/js/sidebarmenu.js"></script>
    <script src="/assets/plugins/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <script src="/assets/plugins/sparkline/jquery.sparkline.min.js"></script>
    <script src="/assets/js/custom.min.js"></script>
    <script src="/assets/plugins/styleswitcher/jQuery.style.switcher.js"></script>
    <script src="/assets/plugins/icheck/icheck.min.js"></script>
    <script src="/assets/plugins/icheck/icheck.init.js"></script>

    <script type="text/javascript">
        $('#example23').DataTable({});
    </script>
</body>
</html>