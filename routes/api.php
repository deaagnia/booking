<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Customer;
use App\Room;
use App\Smember;
use App\Sroom;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//Route::post('/login', 'Auth\\LoginController@login');

Route::get('/customers', function (){
//    return App\Customer::paginate(15);
//    return DB::table('customers')->pagination(10);

    return App\Customer::all();
});

Route::get('/customers/{id}', function ($id){
    return App\Customer::find($id);
});

Route::get('/customers/getByID/{ktp}', function ($ktp){
    $data = DB::table('customers')->where('id_card', '=', $ktp)->first();

    return response()->json($data);
});

Route::post('/customers', function(Request $request) {
    $save = new Customer;
    $save->name = $request->name;
    $save->gender = $request->gender;
    $save->address = $request->address;
    $save->phone_number = $request->phone_number;
    $save->id_card = $request->id_card;
    $save->email = $request->email;
    $save->isMember = false;

    $save->save();
    return $save;
});

Route::put('/customers', function(Request $request) {
    $update = Customer::find($request->id);

    $update->name = $request->name;
    $update->gender = $request->gender;
    $update->address = $request->address;
    $update->phone_number = $request->phone_number;
    $update->id_card = $request->id_card;
    $update->email = $request->email;
    $update->isMember = $request->isMember;

    $update->save();

    return $update;
});

Route::delete('/customers/{id}', function ($id) {
    $delete = Customer::destroy($id);

    return $delete;
});

Route::get('/rooms', function () {
    return Room::all();
});

Route::get('/rooms/{id}', function ($id) {
    return Room::find($id);
});

Route::post('/rooms', function (Request $request) {
    $save = new Room;

    $save->room_number = $request->room_number;
    $save->status      = $request->status;
    $save->price       = $request->price;

    $save->save();
    return $save;
});

Route::put('/rooms', function (Request $request) {
    $id   = $request->id;
    $save = Room::find($id);

    $save->room_number = $request->room_number;
    $save->status      = $request->status;
    $save->price       = $request->price;

    $save->save();
    return $save;
});

Route::delete('rooms/{id}', function ($id) {
    return Room::destroy($id);
});

Route::get('service-member', function () {
    return Smember::all();
});

Route::get('service-member/{id}', function ($id) {
    return Smember::find($id);
});

Route::put('service-member', function (Request $request) {
    $id = $request->id;

    $save = Smember::find($id);

    $save->fasilitas = $request->fasilitas;
    $save->deskripsi = $request->deskripsi;

    $save->save();

    return $save;
});

Route::post('service-member', function (Request $request) {
    $save = new Smember;

    $save->fasilitas = $request->fasilitas;
    $save->deskripsi = $request->deskripsi;

    $save->save();

    return $save;
});

Route::delete('service-member/{id}', function ($id) {
    $del = Smember::destroy($id);

    return $del;
});

Route::get('srooms', function () {
    return Sroom::all();
});

Route::get('srooms/{id}', function ($id) {
    return Sroom::find($id);
});

Route::post('srooms', function (Request $request) {
    $saveSRoom = new Sroom;

    $saveSRoom->room_id     = $request->room_id;
    $saveSRoom->facilities  = $request->facilities;
    $saveSRoom->qty         = $request->qty;
    $saveSRoom->description = $request->description;

    $saveSRoom->save();

    return $saveSRoom;
});

Route::put('srooms', function (Request $request) {
    $id = $request->id;

    $edit = Sroom::find($id);
    $edit->room_id     = $request->room_id;
    $edit->facilities  = $request->facilities;
    $edit->qty         = $request->qty;
    $edit->description = $request->description;

    $edit->save();


    return $edit;
});

Route::delete('srooms/{id}', function ($id) {
    $del = Sroom::destroy($id);
    return $del;
});

