<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class CustomerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        for($i = 0; $i < 20; $i++)
        {
            DB::table('customers')->insert([
               'name'          => $faker->name,
                'gender'       => 'Male',
                'address'      => $faker->address,
                'phone_number' => $faker->phoneNumber,
                'id_card'      => $faker->creditCardNumber,
                'email'        => $faker->email,
                'isMember'     => $faker->word
            ]);
        }
    }
}
