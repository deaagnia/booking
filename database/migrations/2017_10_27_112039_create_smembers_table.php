<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSmembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('smembers', function (Blueprint $table) {
            $table->increments('id');
//            $table->integer('room_id')->unsigned();
            $table->string('fasilitas');
            $table->string('deskripsi');
            $table->timestamps();

//            $table->foreign('room_id')->references('id')->on('rooms');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('smembers');
    }
}
